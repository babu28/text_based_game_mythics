#include "AreaManager.h"

#include <fstream>
#include <time.h>
#include "json.hpp"



std::map<std::string, Area*> AreaManager::retrunAreaPointerMap()
{
	return areaPointerMap;
}

void AreaManager::initialize(std::string filename)
{
	std::ifstream inputStream(filename);
	std::string JSONstr((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON AreasJSON;

	json::JSON AreaListJSON = json::JSON::Load(JSONstr);

		_ASSERT_EXPR(AreaListJSON.hasKey("StartArea"), "Cannot Start without a start Area defined");
		{
			startarea = AreaListJSON["StartArea"].ToString();
		}

		_ASSERT_EXPR(AreaListJSON.hasKey("AreaList"), "There are no rooms defined int he JOSN");
		{
			AreasJSON = AreaListJSON["AreaList"];

			for (auto& it : AreasJSON.ObjectRange())
			{
				Area* area = new Area();
				area->initialize(it.second);
				areaPointerMap.emplace(it.first, area);
			}
		}
}

void AreaManager::saveGame()
{
	std::cout << "Save Game File as a JSON" << std::endl;
}

Area* AreaManager::setStartArea()
{
	if (areaPointerMap.count(startarea) > 0)
	{
		return areaPointerMap[startarea];
	}
}







