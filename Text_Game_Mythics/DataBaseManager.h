#pragma once
#ifndef _DATABASEMANAGER_
#define _DATABASEMANAGER_

#include "sqlite3.h"
class DataBaseManager
{
private:

	int actionCount = 0;
	int pickupCount = 0;
	int gamesPlayedCount = 0;
	int AllyTalkCount = 0;


private:

	inline explicit DataBaseManager()
	{
	}


	inline ~DataBaseManager()
	{
	}
	inline explicit DataBaseManager(DataBaseManager const&)
	{
	}


	inline DataBaseManager& operator=(DataBaseManager const&)
	{
		return *this;
	}

public:

	inline static DataBaseManager& dataBaseManagerInstance() {
		static DataBaseManager instance;
		return instance;
	}

	void actionCountfunc();
	void pickupCountfunc();
	void gamesPlayedCountfunc();
	void AllyTalkCountfunc();
	//void loadDataBase(sqlite3* db);
	void loadDataBase();

};

#endif // !_DATABASEMANAGER_



