#pragma once
#ifndef _ALLY_
#define _ALLY_

#include "ActionItems.h"
class Ally : public ActionItems
{
private:
	std::string AllyName = "";
	std::string questDescription = "";
	bool isQuestGiver = false;

public:
	Ally();
	~Ally();

	void initialize(json::JSON& ally);

	void Talk(std::string& talk) override;
	void QuestTalk(std::string& talk);

	friend class Player;
	friend class Area;
};
#endif // !_ALLY_


