#pragma once
#ifndef _QUESTOBJECT_
#define _QUESTOBJECT_

#include "ActionItems.h"
class QuestObject : public ActionItems
{

private:
	std::string QuestObjectName = "";

public:
	QuestObject();
	~QuestObject();

	void initialize(json::JSON& questobj);

	std::string& PickUp(std::string& questobject) override;

	friend class Player;
	friend class Area;
};
#endif // !_QUESTOBJECT_


