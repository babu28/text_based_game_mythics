#pragma once
#ifndef _PLAYER_
#define _PLAYER_

#include <list>
#include <string>
#include <functional>

#include "AreaManager.h"
#include "ActionItems.h"
#include "Area.h"

class Player
{


private:
	std::map<std::string, std::string> Inventory;
	std::map<std::string, std::string> tempInventory;

	bool hasSkill = false;

	std::map<std::string, std::function<void(Area*)>> lookFunctionMap;

public:
	std::map<std::string, Area*> AreaPointerList;
	std::list<std::string> commandList;
	bool isLOOSE = false;
	bool isWIN = false;
	Area* currentRoom = nullptr;

public:
	Player();
	~Player();


	void Pickup(std::string& pickup);
	void move(std::string& direction);
	void Attack(std::string& attack);
	void talk(std::string& talk);
	void Look(std::string& look);
	void Help(std::string& help);
	void seeInventory(std::string& inventoryInput);
	void collectInventory(std::string& loot);
	void saveGame(std::string& saveGame);



};

#endif // !_PLAYER_

