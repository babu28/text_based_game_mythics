#pragma once
#ifndef _AREAMANAGER_
#define _AREAMANAGER_

#include <map>
#include<list>
#include <string>
#include "Area.h"

class AreaManager
{
private:
	std::map<std::string, Area*> areaPointerMap;
	std::string startarea = "";

private:
	inline explicit AreaManager()
	{
	}


	inline ~AreaManager()
	{
	}
	inline explicit AreaManager(AreaManager const&)
	{
	}

	inline AreaManager& operator=(AreaManager const&)
	{
		return *this;
	}

public:

	std::map<std::string, Area*> retrunAreaPointerMap();

public:
	inline static AreaManager& areaManagerInstance() {
		static AreaManager instance;
		return instance;
	}


	void initialize(std::string filename);
	void saveGame();

	Area* setStartArea();


};

#endif // !_AREAMANAGER_


