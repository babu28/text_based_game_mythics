#pragma once
#ifndef _ACTIONITEMS_
#define _ACTIONITEMS_

#include<string>
#include<iostream>
#include "json.hpp"

class ActionItems
{
public:
	std::string description = "";
	bool isMonster = false;
	bool hasMonster = false;

	ActionItems();
	~ActionItems();
	virtual void initialize(json::JSON& item);

	virtual bool Attack(std::string& mosnterName);
	virtual std::string& PickUp(std::string& item);
	virtual void Talk(std::string& talk);
	virtual void QuestTalk();
};

#endif // !_ACTIONITEMS_




