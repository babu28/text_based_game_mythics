#include "Ally.h"
#include <iostream>

Ally::Ally()
{
}

Ally::~Ally()
{
	//std::cout << "Ally destroyed" << std::endl;

}

void Ally::initialize(json::JSON& ally)
{
	
	if (ally.hasKey("AllyName"))
	{
		 AllyName = ally["AllyName"].ToString();
	}
	if (ally.hasKey("description"))
	{
		description = ally["description"].ToString();
	
	}
	if (ally.hasKey("questDescription"))
	{
		questDescription = ally["questDescription"].ToString();
	}
	if (ally.hasKey("isQuestGiver"))
	{
		isQuestGiver = ally["isQuestGiver"].ToBool();
	}
}

void Ally::Talk(std::string& talk)
{
	if (talk == AllyName)
	{
		std::cout << AllyName << " : " << description << std::endl;
	}
	else
	{
		std::cout << "Cannot Talk to "<< talk << std::endl;
	}
}

void Ally::QuestTalk(std::string& talk)
{
	if (talk == AllyName)
	{
		std::cout<<AllyName <<" : " << questDescription << std::endl;
	}
	else
	{
	std::cout << "Cannot Talk to " << talk << std::endl;
	}
}
