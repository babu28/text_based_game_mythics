#pragma once
#ifndef _COMMANDMANAGER_
#define _COMMANDMANAGER_

#include<map>
#include "Area.h"
#include "Player.h"


class CommandManager
{
private:

	std::map<std::string, std::function<void(Player*, std::string&)>> commandMap;

public:

	Player* player = new Player(); 

	CommandManager();
	~CommandManager();

	void getCommand(std::string& command, std::string& object);

};

#endif // !_COMMANDMANAGER_



