#include "Player.h"
#include "DataBaseManager.h"

Player::Player()
{
	lookFunctionMap.emplace("SKILL", std::function<void(Area*)>(&Area::lookSkill));
	lookFunctionMap.emplace("QUESTITEM", std::function<void(Area*)>(&Area::lookQuestObj));
	lookFunctionMap.emplace("ALLY", std::function<void(Area*)>(&Area::lookAlly));
	lookFunctionMap.emplace("MONSTER", std::function<void(Area*)>(&Area::lookMonster));
	lookFunctionMap.emplace("AREA", std::function<void(Area*)>(&Area::areadescriptionfunc));
}

Player::~Player()
{
	for (auto it = AreaPointerList.begin(); it != AreaPointerList.end(); ++it)
	{
		delete it->second;
		it->second = nullptr;
	}
	//std::cout << "PLayer destroyed" << std::endl;
}


void Player::Attack(std::string& attack)
{
	std::string monsterName = currentRoom->monster->MonsterName;

	if (hasSkill == true && monsterName == attack && monsterName != "")
	{
		bool isGod = currentRoom->monster->Attack(attack);
		if (isGod)
		{
			std::cout << "CONGRULAGATIONS!!! YOU HAVE KILLED GOD" << std::endl;
			std::cout << std::endl << std::endl << "~~~~~~~~~~~~~~~YOU HAVE WON~~~~~~~~~~~~~~~~~~~" << std::endl << std::endl << std::endl;
			isWIN = true;
		}

		//Increment ActionCounter
		DataBaseManager::dataBaseManagerInstance().actionCountfunc();
	}

	else if (hasSkill == true && monsterName != attack)
	{
		std::cout << "Cannot not Attack " << attack << " Sorry MATE!!" << std::endl;
	}

	else if (hasSkill != true && monsterName != attack)
	{
		std::cout << "You do NOT have a skill to Attack" << std::endl;
	}

	else if (hasSkill != true && monsterName == attack && monsterName != "")
	{
		std::cout << "You Dont Have a Skill to Attack With , The " << monsterName << " has Killed You" << std::endl;
		std::cout << std::endl << std::endl << "~~~~~~~~~~~~~~~GAME OVER~~~~~~~~~~~~~~~~~~~" << std::endl << std::endl << std::endl;

		currentRoom->playerDeathInventory = Inventory;
		currentRoom->isDeathRoom = true;
		Inventory.clear();

		isLOOSE = true;
	}
}


void Player::Pickup(std::string& pickup)
{
	if (pickup == "SKILL")
	{
		std::string skill = "";

		try
		{
			skill = currentRoom->skill->PickUp(pickup);
		}
		catch (...)
		{
			std::cout << "Cannot Perform this Action" << std::endl;

		}

		if (skill != "")
		{
			std::cout << "You have Obtained " << skill << std::endl;
			Inventory.emplace(skill, skill);
			hasSkill = true;
			currentRoom->skill->SkillName = "";

			//Increment ActionCounter
			DataBaseManager::dataBaseManagerInstance().actionCountfunc();
			//Increment Pickup Counter
			DataBaseManager::dataBaseManagerInstance().pickupCountfunc();

		}
		else
		{
			std::cout << "Cannot Pick Up " << pickup << " Sorry" << std::endl;
		}
	}
	else if (pickup == "QUESTITEM")
	{
		std::string item = "";
		try
		{
			item = currentRoom->questobj->PickUp(pickup);
		}
		catch (...)
		{
			std::cout << "Cannot Perform this Action" << std::endl;
		}
		if (item != "")
		{
			std::cout << "You Picked Up " << item << std::endl;
			Inventory.emplace(item, item);
			currentRoom->questobj->QuestObjectName = "";

			//Increment ActionCounter
			DataBaseManager::dataBaseManagerInstance().actionCountfunc();
			//Increment Pickup Counter
			DataBaseManager::dataBaseManagerInstance().pickupCountfunc();

		}
		else
		{
			std::cout << "Cannot Pick Up " << pickup << " Sorry" << std::endl;
		}

	}
	else if (pickup != "SKILL" || pickup != "QUESTITEM")
	{
		std::cout << "Cannot Pick Up " << pickup << " Sorry" << std::endl;
	}

}

void Player::talk(std::string& talk)
{

	if ((Inventory.count("COIN") > 0) && currentRoom->ally->isQuestGiver == true)
	{
		currentRoom->ally->QuestTalk(talk);
		Inventory.erase("COIN");

		//Increment ActionCounter
		DataBaseManager::dataBaseManagerInstance().actionCountfunc();
		//Increment AllyTalk Counter
		DataBaseManager::dataBaseManagerInstance().AllyTalkCountfunc();
	}
	else
	{
		currentRoom->ally->Talk(talk);

		//Increment ActionCounter
		DataBaseManager::dataBaseManagerInstance().actionCountfunc();
		//Increment AllyTalk Counter
		DataBaseManager::dataBaseManagerInstance().AllyTalkCountfunc();
	}
}




void Player::Look(std::string& look)
{
	try
	{
		lookFunctionMap[look](currentRoom);
	}
	catch (...)
	{
		std::cout << "Invalid Command!! Please try again" << std::endl;
	}

}


void Player::move(std::string& direction)
{

	std::string Area = currentRoom->Move(direction);

	if (Area != "")
	{
		currentRoom = AreaPointerList[Area];
		currentRoom->areadescriptionfunc();

		//Increment ActionCounter
		DataBaseManager::dataBaseManagerInstance().actionCountfunc();
	}
	else
	{
		std::cout << "You Cannot Perform This Action" << std::endl;

	}

}

void Player::Help(std::string& help)
{

	if (help == "COMMAND")
	{
		for (auto it : commandList)
		{

			std::cout << it << "   [   ]" << std::endl;
		}
	}
	else
	{
		std::cout << "Cannot Do this Action" << std::endl;
	}

}

void Player::seeInventory(std::string& invInpinventoryInputut)
{
	if (invInpinventoryInputut == "INVENTORY" && Inventory.empty() != true)
	{
		std::cout << "---------------You Have-----------------" << std::endl;

		for (auto it = Inventory.begin(); it != Inventory.end(); ++it)
		{
			std::cout << "- " << it->first << std::endl;
		}
	}
	else if (Inventory.empty() == true)
	{
		std::cout << "--------------->>EMPTY<<-----------------" << std::endl;
	}
	else
	{
		std::cout << "Cannot SEE into " << invInpinventoryInputut << " Sorry" << std::endl;
	}

}

void Player::collectInventory(std::string& loot)
{
	if (loot == "INVENTORY" && currentRoom->isDeathRoom == true)
	{
		tempInventory = currentRoom->playerDeathInventory;

		for (auto& it : tempInventory)
		{
			Inventory.emplace(it);
		}
		currentRoom->isDeathRoom = false;
		currentRoom->playerDeathInventory.clear();
		std::cout << "You Have Picked up your Inventory" << std::endl;

		//Increment ActionCounter
		DataBaseManager::dataBaseManagerInstance().actionCountfunc();
	}
	else
	{
		std::cout << loot << "Cannot collect Inventory" << std::endl;
	}
}

void Player::saveGame(std::string& saveGame)
{
	if (saveGame == "GAME")
	{
		AreaManager::areaManagerInstance().saveGame();
	}
	else
	{
		std::cout << "Can NOT save" << saveGame << std::endl;
	}
}






