#include <iostream>
#include <string>
#include <map>
#include <functional>
#include <vector>
#include <algorithm>
#include <sstream>

#include "AreaManager.h"
#include "Player.h"
#include "CommandManager.h"
#include <iterator>
#include "DataBaseManager.h"
#include "sqlite3.h"



void gameLoop()
{

	std::string word = "";

	std::string cmd = "";
	std::string obj = "";

	std::string inputcmd = "";

	CommandManager* command = new CommandManager();
	command->player->AreaPointerList = AreaManager::areaManagerInstance().retrunAreaPointerMap();
	command->player->currentRoom = AreaManager::areaManagerInstance().setStartArea();
	command->player->currentRoom->areadescriptionfunc();

	do
	{
		cmd = "";
		obj = "";
		inputcmd = "";

		std::cout << std::endl << "Enter A Command" << std::endl;

		std::getline(std::cin, inputcmd);
		std::cout << "" <<std::endl;
		std::transform(inputcmd.begin(), inputcmd.end(), inputcmd.begin(), std::toupper);

		if (inputcmd == "EXIT")
		{
			continue;
		}

		//Parse the Input
		std::istringstream iss ( inputcmd );
		std::vector<std::string> inputString (std::istream_iterator<std::string>{iss},std::istream_iterator<std::string>());

		command->getCommand(inputString[0], inputString[1]);

		//Restart Logic
		if (command->player->isLOOSE == true)
		{
			std::cout << "----------------- RESTART [R] ------------------" << std::endl;
			std::cout << "----------------- any other key to EXIT ------------------" << std::endl;

			std::getline(std::cin, inputcmd);
			if (inputcmd == "R")
			{
				std::cout << "\n\n\n\n" <<std::endl;
				std::cout << "-----------------------------------------------------------------\n\n" << std::endl;
				command->player->isLOOSE = false;
				command->player->currentRoom = AreaManager::areaManagerInstance().setStartArea();
				command->player->currentRoom->areadescriptionfunc();

				//Increment gamesPlayed Counter
				DataBaseManager::dataBaseManagerInstance().gamesPlayedCountfunc();

				continue;
			}
		}

	} while (inputcmd != "EXIT" && command->player->isLOOSE != true && command->player->isWIN != true);

	std::cout << "--------------------------------------THANKS FOR PLAYERING--------------------------------" << std::endl;

	delete command;
	command = nullptr;
}


int main()
{
	try
	{
		std::string stdInput = "";

		std::cout << "				NEW GAME [N]			" << std::endl;
		std::cout << "				LOAD GAME [L]			" << std::endl;
		std::getline(std::cin, stdInput);
		std::cout << "\n\n\n\n" << std::endl;

		if (stdInput == "N")
		{
			AreaManager::areaManagerInstance().initialize("MythicsNewGame.json");

			//Increment gamesPlayed Counter
			DataBaseManager::dataBaseManagerInstance().gamesPlayedCountfunc();

			gameLoop();
		}
		else if (stdInput == "L")
		{
			AreaManager::areaManagerInstance().initialize("MythicsLoadGame.json");

			//Increment gamesPlayed Counter
			DataBaseManager::dataBaseManagerInstance().gamesPlayedCountfunc();

			gameLoop();
		}
		else
		{
			std::cout << "Invalid INPUT" << std::endl;
		}

		//DataBase Loading
		DataBaseManager::dataBaseManagerInstance().loadDataBase();
	}

	catch (...)
	{
		std::cout << "ERROR" << std::endl;
	}
}