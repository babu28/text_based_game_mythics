#include "CommandManager.h"

CommandManager::CommandManager()
{
	commandMap.emplace("GO", std::function<void(Player*, std::string&)>(&Player::move));
	commandMap.emplace("ATTACK", std::function<void(Player*, std::string&)>(&Player::Attack));
	commandMap.emplace("PICKUP", std::function<void(Player*, std::string&)>(&Player::Pickup));
	commandMap.emplace("TALK", std::function<void(Player*, std::string&)>(&Player::talk));
	commandMap.emplace("LOOK", std::function<void(Player*, std::string&)>(&Player::Look));
	commandMap.emplace("HELP", std::function<void(Player*, std::string&)>(&Player::Help));
	commandMap.emplace("SEE", std::function<void(Player*, std::string&)>(&Player::seeInventory));
	commandMap.emplace("COLLECT", std::function<void(Player*, std::string&)>(&Player::collectInventory));
	commandMap.emplace("SAVE", std::function<void(Player*, std::string&)>(&Player::saveGame));


	for (auto it = commandMap.begin(); it != commandMap.end(); ++it)
	{
		player->commandList.push_back(it->first);
	}

}

CommandManager::~CommandManager()
{
	delete player;
	player = nullptr;
	//std::cout << "CommandManager destroyed" << std::endl;
}

void CommandManager::getCommand(std::string& command, std::string& object)
{
	try
	{
		commandMap[command](player, object); 

	}
	catch (...)
	{
		std::cout << "Invalid Command!! Please try again" << std::endl;
	}
}

