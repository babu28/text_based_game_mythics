#include "Area.h"
#include <iostream>
#include "json.hpp"
Area::Area() 
{

}

Area::~Area()
{
	delete skill;
	skill = nullptr;
	delete questobj;
	questobj = nullptr;
	delete ally;
	ally = nullptr;
	delete monster;
	monster = nullptr;

	//std::cout << "Area destroyed" << std::endl;
}

void Area::initialize(json::JSON& area)
{
	loadActionItems(area);
}

void Area::areadescriptionfunc()
{
	if (isDeathRoom == true)
	{
		std::cout << AreaDescription << std::endl;
		std::cout << "------------------------------------------------------" << std::endl;
		std::cout << "You had Died here Recently." << std::endl << "Your loot is in this area. You can Pick it Up" << std::endl;
	}
	else
	{
		std::cout << AreaDescription << std::endl;

	}
}


void Area::loadActionItems(json::JSON& areaInfo)
{

	if (areaInfo.hasKey("AreaName"))
	{
		 areaName = areaInfo["AreaName"].ToString();

	}
	if (areaInfo.hasKey("AreaDescription"))
	{
		AreaDescription = areaInfo["AreaDescription"].ToString();

	}

	if (areaInfo.hasKey("actionItems"))
	{
		json::JSON actionItems = areaInfo["actionItems"];

		if (actionItems.hasKey("skills"))
		{
			json::JSON Skills = actionItems["skills"];
			skill->initialize(Skills);

		}

		if (actionItems.hasKey("questobj"))
		{
			json::JSON questOBJ = actionItems["questobj"];
			questobj->initialize(questOBJ);
		}

		if (actionItems.hasKey("allies"))
		{
			json::JSON Allies = actionItems["allies"];
			ally->initialize(Allies);

		}

		if (actionItems.hasKey("monsters"))
		{
			json::JSON Monsters = actionItems["monsters"];
			monster->initialize(Monsters);
		}

	}
	if (areaInfo.hasKey("connectedArea"))
	{
		json::JSON connectedareas = areaInfo["connectedArea"];
		for (auto& it : connectedareas.ArrayRange())
		{
			connectedAreas.emplace(it["direction"].ToString(),it["connectedArea"].ToString());
		}
	}

}


std::string& Area::Move(std::string& dir)
{
	if (connectedAreas.find(dir) != connectedAreas.end())
	{
		connArea = connectedAreas[dir];
		return connArea;
	}
	else
	{
		return connArea;
	}
}

void Area::lookSkill()
{
	if (skill->SkillName != "")
	{
		std::cout<<"This is a " << skill->SkillName << " SKILL"<<std::endl;
		std::cout << skill->description << std::endl;
	}
	else
	{
		std::cout <<" There is no SKILL to look at in this Area" << std::endl;
	}
}

void Area::lookQuestObj()
{
	if (questobj->QuestObjectName != "")
	{
		std::cout << "This is a " << questobj->QuestObjectName << " Quest Object" << std::endl;
		std::cout << questobj->description << std::endl;
	}
	else
	{
		std::cout << "There is no QUEST OBJECT to look at in this Area" << std::endl;
	}

}

void Area::lookAlly()
{
	if (ally->AllyName != "")
	{
		std::cout << "The Allie's Name is " << ally->AllyName << std::endl;
	}
	else
	{
		std::cout << " There is no ALLY to look at in this Area" << std::endl;
	}

}

void Area::lookMonster()
{
	if (monster->MonsterName != "")
	{
		std::cout << "This is the Deadly " << monster->MonsterName << " Monster" << std::endl;
		std::cout << monster->description << std::endl;
	}
	else
	{
		std::cout << " There is no MONSTER to look at in this Area" << std::endl;
	}

}



