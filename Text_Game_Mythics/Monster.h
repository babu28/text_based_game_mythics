#pragma once
#ifndef _MONSTER_
#define _MONSTER_

#include "ActionItems.h"
class Monster : public ActionItems
{
private:
	std::string MonsterName = "";
	bool isGod = false;
	
	
public:

	Monster();
	~Monster();

	void initialize(json::JSON& monster);

	bool Attack(std::string& mosnterName) override;

	friend class Player;
	friend class Area;
};
#endif // !_MONSTER_


