#pragma once
#ifndef _AREA_
#define _AREA_

#include<map>
#include <functional>
#include <string>
#include "json.hpp"

#include "ActionItems.h"
#include "Skill.h"
#include "QuestObject.h"
#include "Ally.h"
#include "Monster.h"
#include <list>
class Area
{
private:

	std::map<std::string, std::string> connectedAreas;

public:

	Skill* skill = new Skill();
	QuestObject* questobj = new QuestObject();
	Ally* ally = new Ally();
	Monster* monster = new Monster();

	std::map<std::string, std::string> playerDeathInventory;

	std::string areaName = "";
	std::string AreaDescription = "";
	std::string connArea = "";

	bool isDeathRoom = false;


public:
	Area();
	virtual ~Area();

	void initialize(json::JSON& area);
	void loadActionItems(json::JSON& areaInfo);

	void areadescriptionfunc();

	std::string& Move(std::string& dir);
	void lookSkill();
	void lookQuestObj();
	void lookAlly();
	void lookMonster();
};

#endif // !_AREA_

