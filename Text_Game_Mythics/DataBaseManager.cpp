#include "DataBaseManager.h"
#include <iostream>

void DataBaseManager::actionCountfunc()
{
	actionCount++;
}

void DataBaseManager::pickupCountfunc()
{
	pickupCount++;
}

void DataBaseManager::gamesPlayedCountfunc()
{
	gamesPlayedCount++;
}

void DataBaseManager::AllyTalkCountfunc()
{
	AllyTalkCount++;
}

void DataBaseManager::loadDataBase()
{
	sqlite3* db;
	char* zErrMsg = 0;
	int rc;

	rc = sqlite3_open("MythicsDB.db", &db);

	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		
	}
	else
	{
		fprintf(stderr, "Opened database successfully\n");
	}

	sqlite3_stmt* statment;
	int Result;

	Result = sqlite3_prepare_v2(db, "INSERT INTO GameAnalysis(noOfActions, noOfPickups,noOfGamesPlayed,noofAllyTalk) values (? , ? , ? , ?)", -1, &statment, 0);
	
	if (Result != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db);
		printf("\nCould not prepare statement.");
		return;
	}

	sqlite3_bind_parameter_count(statment);

	Result = sqlite3_bind_int(statment, 1, actionCount);
	if (Result != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}
	Result = sqlite3_bind_int(statment, 2, pickupCount);
	if (Result != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}
	Result = sqlite3_bind_int(statment, 3, gamesPlayedCount);
	if (Result != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}
	Result = sqlite3_bind_int(statment, 4, AllyTalkCount);
	if (Result != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}

	Result = sqlite3_step(statment);
	if (Result != SQLITE_DONE)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}

	Result = sqlite3_finalize(statment);
	if (Result != SQLITE_DONE)
	{
		std::cout << sqlite3_errmsg(db);
		return;
	}

	sqlite3_close_v2(db);



}
