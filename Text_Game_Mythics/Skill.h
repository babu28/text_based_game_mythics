#pragma once
#ifndef _SKILLS_
#define _SKILLS_

#include "ActionItems.h"

class Skill : public ActionItems
{
private:
	std::string SkillName = "";

public:
	Skill();
	~Skill();

	void initialize(json::JSON& skill);

	std::string& PickUp(std::string& skill) override;

	friend class Player;
	friend class Area;
};
#endif // !_SKILLS_



